# TFM audio long-form


## Trabajo Final de Máster

El uso del recurso sonoro en las producciones multimedia 
de largo formato de la prensa generalista española

Autora: **María Isabel Gómez Gómez**




Tutor: Patrick Urbano
Máster en Periodismo y Comunicación Digital: Datos y Nuevas Narrativas
Universitat Oberta de Catalunya (UOC)
Enero 2023




